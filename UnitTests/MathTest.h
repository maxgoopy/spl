#pragma once
#include "pch.h"
#include "SPL_Math.h"

TEST(MathTest, NumberContainDigit)
{
	EXPECT_TRUE(SPL_Math::NumberContainsDigit(4815, 4));
	EXPECT_TRUE(SPL_Math::NumberContainsDigit(1623, 1));
	EXPECT_TRUE(SPL_Math::NumberContainsDigit(42, 2));


	EXPECT_FALSE(SPL_Math::NumberContainsDigit(999, 7));
	EXPECT_FALSE(SPL_Math::NumberContainsDigit(777, 9));
}
