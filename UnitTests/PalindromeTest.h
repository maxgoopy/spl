#pragma once
#include "pch.h"
#include"SPL_Palindrome.h"
#include <vector>

TEST(PalindromeTest, StringArg) {
	std::string str("tenet");
	EXPECT_TRUE(IsPalindrome(&str));

	str = "A man, a plan, a canal - Panama";
	EXPECT_TRUE(IsPalindrome(&str));
}

TEST(PalindromeTest, CStringArg) {
	EXPECT_TRUE(IsPalindrome("tenet"));
	EXPECT_TRUE(IsPalindrome("Madam, I'm Adam"));
	EXPECT_TRUE(IsPalindrome("Te n Et!"));
	EXPECT_FALSE(IsPalindrome("No tenet?"));

}

TEST(PalindromeTest, VectorArg) {
	std::vector<char> cvec{ 'T','N','T' };
	EXPECT_TRUE(IsPalindrome(&cvec));

	cvec.clear();
	cvec = { 'N','O','U','P' };
	EXPECT_FALSE(IsPalindrome(&cvec));

}
