#pragma once
#include "pch.h"
#include "SPL_FibonacciNumber.h"

TEST(FibonacciTest, GetValue)
{
	EXPECT_EQ(0, SPL_FibonacciNumber::GetFibonacciNumber(0));
	EXPECT_EQ(1, SPL_FibonacciNumber::GetFibonacciNumber(1));
	EXPECT_EQ(1, SPL_FibonacciNumber::GetFibonacciNumber(2));
	EXPECT_EQ(2, SPL_FibonacciNumber::GetFibonacciNumber(3));
	EXPECT_EQ(3, SPL_FibonacciNumber::GetFibonacciNumber(4));
	EXPECT_EQ(5, SPL_FibonacciNumber::GetFibonacciNumber(5));
}

TEST(FibonacciTest, InvalidIndex)
{
	EXPECT_EQ(0, SPL_FibonacciNumber::GetFibonacciNumber(-1));
}