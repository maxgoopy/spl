#include "pch.h"
#include "FibonacciNumberTest.h"
#include "MathTest.h"
#include "PalindromeTest.h"

int main(int argc, char** argv) {
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
