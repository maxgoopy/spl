#pragma once
namespace SPL_Math
{
	/**
	 * If given Number contains given Digit
	 */
	bool NumberContainsDigit(int Number, unsigned int Digit);

	/**
	 * Revert given Number
	 */
	unsigned int Revert(unsigned int Number);
}

