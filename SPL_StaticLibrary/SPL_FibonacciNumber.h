#pragma once
#include <vector>
#include <functional>

class SPL_FibonacciNumber
{
	//Container of calculated Fibonacci numbers
	static std::vector<int>* FNumbers;
	
	//Initialize default values for FNumbers
	static void InitFNumbers();
public:
	/** Get Fibonacci Number with current index
	* @param index - Index of Fibonacci Number
	* @return Value of Fibonacci Number
	*/
	static int GetFibonacciNumber(int index);

	/** Calculate all of Fibonacci number from 0 to maxIndex and filtere them.
	 * In output vector will be added filtered values.
	 * @param maxIndex - maximum index of  Fibonacci number
	 * @param output - vector of int in which all filtered values will be added
	 * @param filter - value filter
	 */
	static bool GetFilteredNumber(int maxIndex, std::vector<int>& output, std::function<bool(int)> filter);
};

