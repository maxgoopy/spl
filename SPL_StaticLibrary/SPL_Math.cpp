#include "pch.h"
#include "SPL_Math.h"
#include <string>

bool SPL_Math::NumberContainsDigit(int Number, unsigned int Digit)
{
	auto strNumber = std::to_string(Number);
	auto strDigit = std::to_string(Digit);

	return strNumber.find(strDigit) != std::string::npos;
}

unsigned int SPL_Math::Revert(unsigned int Number)
{
	std::string original = std::to_string(Number);
	std::string reversed;

	for (std::string::reverse_iterator iter = original.rbegin(); iter != original.rend(); ++iter)
	{
		reversed += *iter;
	}
	return std::stoi(reversed);
}
