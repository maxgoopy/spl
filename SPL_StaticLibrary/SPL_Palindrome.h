#pragma once
#include <string>
/**
 * If given sequence of char can be determinate as Palindrom
 * Container items must be char type
 * If it contain letters then function return false
 * \param container - container of char
 * \return true if given sequence of char can be determinate as Palindrom
 */
template<typename T>
bool IsPalindrome(const T* container) {
	static_assert(std::is_same<T::iterator::value_type, char>::value, "Container type must be char");

	typename T::const_iterator iterF = container->begin();
	typename T::const_iterator iterR = container->end();

	if (iterF == iterR)
		return false;
	--iterR;

	bool hasChar = false;
	while (iterF < iterR)
	{
		if (!isalpha(*iterF))
		{
			++iterF;
			continue;
		}

		if (!isalpha(*iterR))
		{
			--iterR;
			continue;
		}

		hasChar = true;

		if (tolower(*iterF) != tolower(*iterR))
			return false;
		++iterF;
		--iterR;
	}

	return hasChar;
	return false;
};

template<>
bool IsPalindrome<char>(const char* arg) {
	std::string str(arg);
	return IsPalindrome(&str);
}
