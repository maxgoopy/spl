#include "pch.h"
#include "SPL_FibonacciNumber.h"

std::vector<int>* SPL_FibonacciNumber::FNumbers = nullptr;

void SPL_FibonacciNumber::InitFNumbers()
{
	if (FNumbers != nullptr)
		delete FNumbers;

	FNumbers = new std::vector<int>{ 0,1 };
}

int SPL_FibonacciNumber::GetFibonacciNumber(int index)
{
	if (index < 0)
		return 0;

	//Init FNumbers if required
	if (FNumbers == nullptr)
		InitFNumbers();

	//Already have required value
	if (FNumbers->size() >= index + 1)
		return FNumbers->at(index);

	//Calculate required value
	for (int i = FNumbers->size(); i <= index; ++i)
	{
		FNumbers->push_back((*FNumbers)[i - 2] + (*FNumbers)[i - 1]);
	}

	return FNumbers->back();
}

bool SPL_FibonacciNumber::GetFilteredNumber(int maxIndex, std::vector<int>& output, std::function<bool(int)> filter)
{
	//Create required FNumbers
	GetFibonacciNumber(maxIndex);

	bool filteredNumberExist = false;

	//Filtering numbers
	for (int i = 0; i <= maxIndex; ++i)
		if (filter(FNumbers->at(i)))
		{
			output.push_back(FNumbers->at(i));
			filteredNumberExist = true;
		}

	return filteredNumberExist;
}
