
#include <iostream>
#include <string>
#include <vector>
#include"SPL_Palindrome.h"
#include"SPL_FibonacciNumber.h"
#include"SPL_Math.h"

int main()
{
	//Function for Palindrome check
	std::cout << "\"Madam, I'm Adam\"" << " Is a Palindrome?" << std::endl;
	std::cout << (IsPalindrome("Madam, I'm Adam") ? "true" : "false");
	std::cout << std::endl << std::endl;
	std::cout << "\"Teenage mutant ninja turtles\"" << " Is a Palindrome?" << std::endl;
	std::cout << (IsPalindrome("Teenage mutant ninja turtles") ? "true" : "false");
	std::cout << std::endl << std::endl;

	//Generate first 30 Fibonacci numbers and select those that contain a digit one
	std::vector<int> numbers;
	SPL_FibonacciNumber::GetFilteredNumber(30, numbers, [](int arg) { return !SPL_Math::NumberContainsDigit(arg, 1); });

	std::cout << "Generate first 30 Fibonacci numbers and select those that not contain a digit one :" << std::endl;
	for(auto num : numbers)
		std::cout << num << "  ";

	std::cout << std::endl << std::endl;

	numbers.clear();
	SPL_FibonacciNumber::GetFilteredNumber(40, numbers, [](int arg) { return SPL_Math::NumberContainsDigit(arg, 7); });

	std::cout << "Generate first 40 Fibonacci numbers and select those that contain a digit seven :" << std::endl;
	for (auto num : numbers)
		std::cout << num << "  ";

	std::cout << std::endl << std::endl;

	//Revert int number 
	int origin = 32589;
	std::cout << "Original number: " << origin << std::endl;
	std::cout << "Reverted number: " << SPL_Math::Revert(origin);
	std::cout << std::endl << std::endl;

	origin = 88888;
	std::cout << "Original number: " << origin << std::endl;
	std::cout << "Reverted number: " << SPL_Math::Revert(origin);
	std::cout << std::endl << std::endl;

	return 0;
}
